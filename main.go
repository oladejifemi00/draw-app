package main

import (
	"errors"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"time"
)


type Team struct{
	Name string
	Cohort int
	Seeded bool
}

type Competetion struct{
	Name string
	Theme string
	Teams []Team
}

/**
	seeded teams - 
	Cohort 8 (Phoenix)- defending champions
	Cohort 10 (Unknown) - runners up/finalists
	cohort 11 (Uknown)- best losers(group)

	unseeded teams-
	Cohort 9 (Technophiles)
	Cohort 12 (Unnamed)
	Cohort 13 (Paragons)

	*/


	var allteams []Team
	var drawnTeams []Team
	var phoenix =Team{
		Name: "PHOENIX",
		Cohort:8,
		Seeded:true,
	}

	var technophiles =Team{
		Name: "TECHNOPHILES",
		Cohort:9,
		Seeded:false,
	}

	var sages =Team{
		Name: "SAGES",
		Cohort:10,
		Seeded:true,
	}

	var lavish =Team{
		Name: "LAVISH",
		Cohort:11,
		Seeded:true,
	}

	var dorobucci =Team{
		Name: "DOROBUCCI",
		Cohort:12,
		Seeded:false,
	}

	var paragon =Team{
		Name: "PARAGON",
		Cohort:13,
		Seeded:false,
	}

	
func main()  {
	allteams = append(allteams, phoenix, technophiles, sages, lavish, dorobucci, paragon)
	comp:= &Competetion{
		Name:"THE PHOENIX ",
		Theme: "RISING OF THE PHOENIX ",
		Teams: allteams,
	}
	fmt.Printf(comp.Greeting(), comp.Theme)
	// loading()
	fmt.Printf(Prompt(), "Draw Team", "Exit")
	input := takeInput()
	loading()
	for !isValidInput(input){
		fmt.Printf("%T", input)
		fmt.Println(input)
		fmt.Println("Get Sense, enter 1 or 2")
		input = takeInput()
	}

	comp.displaySeeded()
	comp.draw()
	displayUnseeded()

	clearScreen()

}

func (competetion *Competetion) AddTeam(team *Team)  (string, error){
	initialNumberOfTeams := len(competetion.Teams)
	competetion.Teams=append(competetion.Teams, *team)
	if (initialNumberOfTeams>=len(competetion.Teams)){
		return "",errors.New("error adding team")
	}
	return "team added successfully",nil
}

func (competetion *Competetion) RemoveTeam(team *Team)  (string, error){
	initialNumberOfTeams := len(competetion.Teams)
	newTeams:= []Team{}
	for _, cohort := range competetion.Teams {
		if cohort !=*team{
			newTeams = append(newTeams, cohort)
		}
	}
	competetion.Teams = newTeams
	if len(competetion.Teams)!=initialNumberOfTeams-1 {
		return "",errors.New("cohort " + strconv.Itoa(team.Cohort) +" no gree go house ooo")

	}
	return "team removed successfully", nil
}

func (competetion *Competetion) getSeededTeams() []Team{
	var seededTeams []Team
	for _, team := range competetion.Teams {
		if team.Seeded{
			seededTeams = append(seededTeams, team)
		}
	}
	return seededTeams
}

func (competetion *Competetion) getUnseededTeams() []Team{
	var unseededTeams []Team
	for _, team := range competetion.Teams {
		if !team.Seeded{
			unseededTeams = append(unseededTeams, team)
		}
	}
	return unseededTeams
}

func (competetion *Competetion) Greeting() string{
	return `
	
	----------------------------------------------------------------------------------------
	|                               %s                                  |
	----------------------------------------------------------------------------------------
			TEAMS                                
	1. COHORT 8 (PHOENIX)						  
	2. COHORT 9 (TECHNOPHILES)                    
	3. COHORT 10 (SAGES)                          
	4. COHORT 11 (LAVISH)                         
	5. COHORT 12 (DOROBUCCI)                      
	6. COHORT 13 (PARAGON)                         

			SEEDED TEAMS					     UNSEEDED TEAMS
		    COHORT 8 (PHOENIX)			               COHORT 9 (TECHNOPHILES)
		    COHORT 10 (SAGES)			               COHORT 12 (DOROBUCCI)
		    COHORT 11 (LAVISH)                                 COHORT 13 (PARAGONS)
`
}

func (competition *Competetion) draw() {  
	for len(drawnTeams) !=3 {
		rand.Seed(time.Now().UnixNano())
		num:=rand.Intn(len(competition.getUnseededTeams()))
		log.Println("random num->", num)
		drawnTeam:=drawnTeams[num]
		log.Println("ddr->", drawnTeam)
		if competition.validateDrawnTeam(drawnTeam){
			drawnTeams=append(drawnTeams, drawnTeam)
			log.Println("drawn -->", drawnTeam)
		}
	}
	log.Println("teams drawn", drawnTeams)
}

func Prompt() string{
	return `
	1. %s
	2. %s
	`
}

func loading(){
	fmt.Print("loading")
	for i := 0; i < 5; i++ {
		time.Sleep(1 * time.Second) 
		fmt.Print(".")
	}
}

func clearScreen() {
	fmt.Print("\033[H\033[2J")
}

func takeInput() int{
	var option int
	_, err:=fmt.Scanln(&option)
	if err!=nil{
		clearScreen()
		Prompt()
		takeInput()
	}
	return option
}

func isValidInput(input int) bool{
	fmt.Println(input==1, input==2)
	if input!=1 && input!=2{
		return false
	}
	return true
}

func(competition *Competetion) validateDrawnTeam(drawnTeam Team) bool{
	for _, team := range drawnTeams{
		if drawnTeam.Seeded && team==drawnTeam{
			return false
		}
	}
	return true
}

func (competetion *Competetion) displaySeeded()  {
	for i := 0; i < len(competetion.getSeededTeams()); i++ {
		fmt.Printf("%v	\t\t", "COHORT " + strconv.Itoa(competetion.getSeededTeams()[i].Cohort))
	}
	fmt.Print("\n")
	for i := 0; i < len(competetion.getSeededTeams()); i++ {
		fmt.Printf("%s	\t\t\t", "VS")
	}	
	fmt.Println()
}

func displayUnseeded(){
	// var displayCount int
	for i := 0; i < len(drawnTeams); i++ {
		time.Sleep(2*time.Microsecond)
		fmt.Printf("%v	\t\t", "COHORT " + strconv.Itoa(drawnTeams[i].Cohort))
	}
}