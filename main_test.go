package main

import (
	"log"
	"testing"
)

var competetion Competetion
var team1, team2 *Team

func setUp(){
	competetion=Competetion{
		Name: "Phoenix Graduation Ceremony",
		Theme: "Rising Of The Phoenix",
	}
	
	team1 =&Team{
		Name: "Phoenix",
		Cohort: 8,
		Seeded: true,
	}

	team2 =&Team{
		Name: "Technophiles",
		Cohort: 9,
		Seeded: false,
	}
}

func TestAddTeamToCompetetion(t *testing.T) {
	setUp()
	msg, err:=competetion.AddTeam(team1)
	if err!=nil{
		t.Error("test failed")
	}
	log.Println(msg)

	log.Println(competetion)
}


func TestRemoveTeam(t *testing.T) {
	setUp()
	competetion.AddTeam(team1)
	competetion.AddTeam(team2)
	if len(competetion.Teams) <2 {
		t.Error("test failed")
	}
	log.Println(competetion)
	
	msg, err:=competetion.RemoveTeam(team1)
	if err!=nil{
		log.Println("err: ", err)
		t.Error("remove failed")
	}
	log.Println(competetion)
	log.Println(msg)
}